module.exports = {
	rethinkdb:{
		host: process.env.DB_HOST || 'localhost', // indirizzo per la connessione database
		port: process.env.DB_PORT || 28015, 	  // porta per la connessione al database
		db: process.env.DB_NAME || 'WebLevels',   // nome del database                           
	},
	express:{
		port:process.env.DB_SITE_PORT || 3000  // porta in ascolto del sito
	}
}
