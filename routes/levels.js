// librerie utilizzate
var express = require('express');
var router = express.Router();
var api = require('../models/api'); // libreria creata per la gestione del database
var passport = require('../models/api').passport; // libreria che gestisce l'autenticazione


router.get('/web1/login', function(req, res) {
    res.render('../login');
});
router.get('/web2/login', function(req, res) {
    res.render('../login');
});
router.get('/web3/login', function(req, res) {
    res.render('../login');
});
router.get('/web4/login', function(req, res) {
    res.render('../login');
});

router.get('/web1', function(req, res, err) {
    console.log("sono entreto in web1", req.user);
    if (req.user) {
        if (req.user.username == "web1") {
            res.render('../web1');
        }
    } else {
        res.redirect('/web1/login');
    }
});

router.get('/web2', function(req, res, err) {
    if (req.user) {
        if (req.user.username == "web2") {
            res.render('../web2');
        }
    } else {
        res.redirect('/web2/login');
    }
});

router.get('/web3', function(req, res, err) {
    if (req.user) {
        if (req.user.username == "web3") {
            res.render('../web3');
        }
    } else {
        res.redirect('/web3/login');
    }
});


router.get('/files', function(req, res, err) {
    if (req.user) {
        if (req.user.username == "web3") {
            res.render('../files');
        }
    } else {
        res.redirect('/web3/login');
    }
});

router.get('/web4', function(req, res, err) {
    if (req.user) {
        if (req.user.username == "web4") {
            if (req.cookies.loggedIn == 0) {
                res.render('../web4');
            } else {
                res.render('../web4-1');
                // res.cookie("loggedIn", 0);
            }
        }
    } else {
        res.redirect('/web4/login');
    }
});


// Route che si attiva quando l'utente fa la richiesta di post sulla pagina /login
router.post('/web1/login', passport.authenticate('local', { successRedirect: '/web1', failureRedirect: '/web1/login', failureFlash: true }));
router.post('/web2/login', passport.authenticate('local', { successRedirect: '/web2', failureRedirect: '/web2/login', failureFlash: true }));
router.post('/web3/login', passport.authenticate('local', { successRedirect: '/web3', failureRedirect: '/web3/login', failureFlash: true }));
router.post('/web4/login', passport.authenticate('local', { successRedirect: '/web4', failureRedirect: '/web4/login', failureFlash: true }),
    function(req, res, err) { res.cookie("loggedIn", 0); });

module.exports.levels = router;