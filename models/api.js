// var rdb = require('rethinkdb');
var config = require('../config/config');
var passport = require('passport');
var bcrypt = require('bcryptjs');
var LocalStrategy = require('passport-local').Strategy;
var thinky = require('thinky')({
	host: config.rethinkdb.host,
	port: config.rethinkdb.port,
	db: config.rethinkdb.db,
	// user: config.rethinkdb.user,
	// password: config.rethinkdb.password
});

var type = thinky.type;
var r = thinky.r;
var Errors = thinky.Errors;

var Query = thinky.Query;
//modelli relativi alle tabelle
var User = thinky.createModel('utenti', {
	id: type.string(),
	username: type.string(),
	password: type.string()
});

User.ensureIndex('id');

// PASSPORT STRATEGIES
passport.use(new LocalStrategy(
	function(username, password, done) {
		User.filter(r.row('username').eq(username)).nth(0).default("").execute().then(function(user) {
				if (user.length == 0) {
					return done(null, false, { message: 'Username o Password errati. Si prega di inserire un username valido e la corrispondente password.' });
				} else {

					if (!bcrypt.compareSync(password, user.password)) {

						return done(null, false, { message: 'Username o Password errati. Si prega di inserire un username valido e la corrispondente password.' });
					}
					console.log('utente ', user.username, ' si è appena loggato');
					return done(null, user);
				}
			})
			.error(function(err) {
				return done(err);
			});
	}
));

passport.serializeUser(function(user, done) {
	done(null, user.id);
});

passport.deserializeUser(function(id, done) {
	r.table("utenti").get(id).without("password").run()
		.then(function(user) {
			done(null, user);
		})
		.error(function(err) {
			console.log("ci sta un errore", err);
		});
});

exports.passport = passport;
