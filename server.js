var express = require('express'); // modulo per il framework express
var path = require('path'); // modulo che permette la gestione dei percorsi del sistema operativo
var cookieParser = require('cookie-parser'); // modulo per il parsing dei cookie
var bodyParser = require('body-parser'); // modulo per il parsing del body delle pagine html
var fs = require('fs'); // modulo per la gestione dei file
var config = require('./config/config'); // file config contente le informazioni necessarie per la confiurazione

var expressValidator = require('express-validator'); // modulo per la convalida dei campi per il login e la registrazione
var flash = require('connect-flash'); // modulo per l'invio dei messaggi flash relativi agli errori o successi
var session = require('express-session'); // modulo per la gestione delle sessioni
var redis = require("redis"); // modulo per l'interazione con il database redis

var redisStore = require('connect-redis')(session); // modulo per che permette il salvataggio delle sessioni 
// sul database redis
var client = redis.createClient(); // istanza per l'interazione con il database redis
var passportSocketIo = require('passport.socketio'); // modulo per utilizzare il modulo passport nel modulo socket.io
var api = require("./models/api.js"); // file per richiamare le query necessarie dal database rethinkdb

var passport = api.passport; // richiama il modulo passport che serve all'autenticazione dell'utente 

var expressLayouts = require("express-ejs-layouts"); // modulo del framework express per i template ejs
// I file che gestiscono il routing del web server
var routes = require('./routes/levels');
// var users = require('./routes/users');

// Inizializzazione dell'applicazione
var app = express();
// sono le opzioni necessarie per fare utilizzare il protocollo https

// creazione del server con il modulo https
var server = require('http').createServer(app);

// Motore dei template
app.use(expressLayouts);
app.set('views', path.join(__dirname, '/views/layouts'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

// BodyParser è una funzione middleware necessaria per poter prendere in maniera chiara i parametri
// del body delle pagine html
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser('secret'));

// viene detto al server di rendere visibile la cartella public al lato client
app.use(express.static(path.join(__dirname, 'public')));
app.use('/web4', function(req, res, next) {
    var cookie = req.cookies.loggedIn;
    if (cookie === undefined) {
        // no: set a new cookie
        var randomNumber = Math.random().toString();
        randomNumber = randomNumber.substring(2, randomNumber.length);
        res.cookie('loggedIn', 0, { maxAge: 900000, httpOnly: false });
        console.log('cookie created successfully');
    } else {
        // yes, cookie was already present 
        console.log('cookie exists', cookie);
    }
    next(); // <-- importan
});

// Express session
// Serve per salvare le sessioni sul database redis e assegna a ciascuna di esse 
// un tempo di 30 minuti prima che essa scade  
var sessionStore = new redisStore({ host: 'localhost', port: 6379, client: client, unset: "destroy" });
var sessionMiddleware = session({
    name: "connect_id",
    secret: "secret",
    cookie: {
        maxAge: 30 * 60 * 1000,
    },
    store: sessionStore,
    saveUninitialized: false,
    resave: false,
    rolling: true
});
// dice al web server di utilizzare le sessioni precedentemente dichiarate
app.use(sessionMiddleware)

// Viene inizializzato il modulo passport e gli viene detto di utilizzaare le sessioni precedentemente dichiarate
app.use(passport.initialize());
app.use(passport.session());

// dice al modulo express-validator di utilizzare una funzione errore costumizzata 
app.use(expressValidator({
    errorFormatter: function(param, msg, value) {
        var namespace = param.split('.'),
            root = namespace.shift(),
            formParam = root;

        while (namespace.length) {
            formParam += '[' + namespace.shift() + ']';
        }
        return {
            param: formParam,
            msg: msg,
            value: value
        };
    }
}));

// Connect Flash
app.use(flash());

var connectedUsers = 0;

// Variabili globali per il server e il client
app.use(function(req, res, next) {
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error = req.flash('error');
    res.locals.user = req.user || null;
    next();
})

app.use('/', routes.levels); // dice al web server di utilizzare i route nel file /routes/index.js
// app.use('/', users);  // dice al web server di utilizzare i route nel file /routes/users.js

// route che invia il 404 not found quando si tenta di accedere ad un percorso non gestito o inesistente
app.use(function(req, res) {
    res.status(404);
    res.sendFile(__dirname + '/views/404.html');
});

// setta la porta di ascolto del web server
app.set('port', (process.env.PORT || config.express.port || 3000));

// Avvio del server
server.listen(app.get('port'), function() {
    console.log('Server started on port ' + app.get('port'));
})

client.flushdb(function(error, succeeded) {
    console.log(succeeded);
});